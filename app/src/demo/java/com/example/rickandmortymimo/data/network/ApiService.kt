package com.example.rickandmortymimo.data.network

import com.example.rickandmortymimo.data.domain.CharacterResponse
import com.example.rickandmortymimo.data.domain.EpisodeResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("character")
    suspend fun getCharacters(@Query("page") type: Int): CharacterResponse

    @GET("episode")
    suspend fun getEpisodes(@Query("page") type: Int): EpisodeResponse


}







