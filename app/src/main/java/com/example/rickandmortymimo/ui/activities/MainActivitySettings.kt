package com.example.rickandmortymimo.ui.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.ui.custom.MyToolBar
import kotlinx.android.synthetic.main.activity_settings.*

class MainActivitySettings : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        val sharedPref = this.getSharedPreferences("myPrefs",Context.MODE_PRIVATE) ?: return
        val themePref = sharedPref.getInt(getString(R.string.theme_preference), -1)

        when (themePref) {
            1 -> {
                setTheme(R.style.ToolbarTheme_Rickandmortymimo)
            }
            0 -> {
                setTheme(R.style.Theme_Rickandmortymimo)
            }
            else -> {
                print("valor es -1")
            }
        }

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_settings)

        MyToolBar().show(this, getString(R.string.label_settings), true)



        if (themePref == 0) {
            setTheme(R.style.ToolbarTheme_Rickandmortymimo)
            switch1.text = getString(R.string.switch_dark)
            switch1.isChecked = true
        } else {
            switch1.text = getString(R.string.switch_light)
        }

        switch1.setOnCheckedChangeListener { _ , isChecked ->
            if (isChecked) {
                switch1.text = getString(R.string.switch_dark)
                with (sharedPref.edit()) {
                    putInt(getString(R.string.theme_preference), 0)
                    apply()

                    onRestart()
                }

                Toast.makeText(applicationContext, getString(R.string.warn_settings), Toast.LENGTH_LONG).show()

            } else {
                switch1.text = getString(R.string.switch_light)
                with (sharedPref.edit()) {
                    putInt(getString(R.string.theme_preference), 1)
                    apply()
                    onRestart()
                }

                Toast.makeText(applicationContext, getString(R.string.warn_settings), Toast.LENGTH_LONG).show()
            }
        }

    }



    }



