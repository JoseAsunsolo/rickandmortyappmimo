package com.example.rickandmortymimo.ui.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.rickandmortymimo.data.domain.CharacterModel
import com.example.rickandmortymimo.data.network.repository.CharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class CharactersViewModel @Inject constructor(private val repository: CharacterRepository): ViewModel() {

    fun getListData() : Flow<PagingData<CharacterModel>> {
        return repository.getCharacter().cachedIn(viewModelScope)
    }




}
