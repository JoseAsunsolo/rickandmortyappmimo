package com.example.rickandmortymimo.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.ui.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_view_pager.*

class ViewPagerFragment : Fragment() {

    private val URL_AMAZON = "https://www.amazon.es/s?k=rick+y+morty&i=toys&__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss_1"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_view_pager, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imgs = listOf(R.drawable.producto1,R.drawable.producto2,R.drawable.producto3,R.drawable.producto4, R.drawable.producto5, R.drawable.producto6 )

        btnAmazon.setOnClickListener{
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.addCategory(Intent.CATEGORY_BROWSABLE)
                intent.data = Uri.parse(URL_AMAZON)
                ContextCompat.startActivity(requireActivity(), intent, Bundle())

        }

        val adapter = ViewPagerAdapter(imgs,requireActivity())
        pager.adapter = adapter

    }


}