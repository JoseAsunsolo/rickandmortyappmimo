package com.example.rickandmortymimo.ui.fragments.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.rickandmortymimo.R
import kotlinx.android.synthetic.main.fragment_episodes_details.*


class EpisodesDetailsFragment: Fragment(R.layout.fragment_episodes_details) {

    private val URL_HBO = "https://es.hboespana.com/series/rick-y-morty/3be6e070-1044-4740-8478-2936de705e4c?gclid=CjwKCAjw55-HBhAHEiwARMCsztbmLM4qDWUZcGew1JF4fG2RBcZJJpgRNkeR3LSCBDeXXH236cL4OBoCx18QAvD_BwE&gclsrc=aw.ds"


    private val args: EpisodesDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val episode = args.episode

        tvNameEpisodeDetails.text= episode.episode_number

        tvEpisodeNumberDetails.text= episode.name

        btnHBO.setOnClickListener{
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            intent.data = Uri.parse(URL_HBO)
            ContextCompat.startActivity(requireActivity(), intent, Bundle())

        }


    }
}