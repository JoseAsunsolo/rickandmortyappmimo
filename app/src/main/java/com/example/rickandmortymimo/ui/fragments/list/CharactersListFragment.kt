package com.example.rickandmortymimo.ui.fragments.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.databinding.FragmentListBinding
import com.example.rickandmortymimo.ui.adapters.CharacterAdapter
import com.example.rickandmortymimo.ui.viewmodel.CharactersViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CharactersListFragment : Fragment(R.layout.fragment_list) {


    private lateinit var binding: FragmentListBinding
    private lateinit var recyclerAdapter: CharacterAdapter
    private var searchJob: Job? = null
    private val charactersVM: CharactersViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(layoutInflater)

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerAdapter = CharacterAdapter()
        startSearchJob()
        setUpAdapter()

        srlRefreshCharacters.setOnRefreshListener {
            srlRefreshCharacters?.isRefreshing = true
            startSearchJob()
        }


    }

    private fun startSearchJob() {

        searchJob?.cancel()

        searchJob = lifecycleScope.launch {
            charactersVM.getListData()
                .collectLatest {
                    recyclerAdapter.submitData(it)
                }

        }
        srlRefreshCharacters?.isRefreshing = false
    }

    private fun setUpAdapter() {

        binding.rvCharacters.apply {
            adapter = recyclerAdapter
        }

        recyclerAdapter.addLoadStateListener { loadState ->

            if (loadState.refresh is LoadState.Loading) {

                if (recyclerAdapter.snapshot().isEmpty()) {
                    binding.progressIndicator.isVisible = true
                }
                binding.errorTextView.isVisible = false

            } else {
                binding.progressIndicator.isVisible = false

                val error = when {
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error

                    else -> null
                }
                error?.let {
                    if (recyclerAdapter.snapshot().isEmpty()) {
                        binding.errorTextView.isVisible = true
                        binding.errorTextView.text = getString(R.string.error_internet_gone)
                    }
                }

            }
        }
    }


}