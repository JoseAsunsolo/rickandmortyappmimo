package com.example.rickandmortymimo.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.rickandmortymimo.R
import kotlinx.android.synthetic.main.fragment__custom_info.view.*

@SuppressLint("CustomViewStyleable", "UseCompatLoadingForDrawables")
class InfoCustomView @JvmOverloads constructor(context: Context,
                                               attrs: AttributeSet? = null,
                                               defStyle: Int = 0,
                                               defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    init {
        LayoutInflater.from(context).inflate(R.layout.fragment__custom_info, this, true)
        orientation = VERTICAL

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it,
                R.styleable.custom_component_attributes, 0, 0)
            val title = resources.getText(typedArray
                .getResourceId(R.styleable
                    .custom_component_attributes_custom_component_title, R.string.about_info))

            tvTitleInfo.text = title
            tvName_1.text = resources.getString(R.string.name_1)
            tvName_2.text = resources.getString(R.string.name_2)
            tvCopyText.text = resources.getString(R.string.copy_text)

            typedArray.recycle()
        }


    }

}