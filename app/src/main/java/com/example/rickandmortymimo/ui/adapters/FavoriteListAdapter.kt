package com.example.rickandmortymimo.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.data.db.entities.CharacterEntity
import com.example.rickandmortymimo.ui.fragments.list.FavoritesFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_favorites.view.*

class FavoriteListAdapter(private val fragmentFav: FavoritesFragment) : RecyclerView.Adapter<FavoriteListAdapter.ViewHolder>() {
    private val characters by lazy { mutableListOf<CharacterEntity>() }


    fun setCharacters(characterEntities: List<CharacterEntity>) {
        if (characterEntities.isNotEmpty()) {
            this.characters.clear()
        }

        this.characters.addAll(characterEntities)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_list_favorites, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        holder.bind(character)

        holder.btnDelete.setOnClickListener{
            removeWord(position)
            fragmentFav.deleteFav(character)


        }

    }

    override fun getItemCount(): Int {
        return characters.size
    }



    private fun removeWord(adapterPosition: Int) {
        characters.removeAt(adapterPosition)
        notifyItemRemoved(adapterPosition)
    }





    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val btnDelete: Button = itemView.findViewById(R.id.btnDelete)

        fun bind(characterEntities: CharacterEntity) {
            with(itemView) {

                Picasso.get().load(characterEntities.image).into(imageCharFav)

                characterNameFav.text = characterEntities.name
                stateTitleFav.text = characterEntities.status


                if (characterEntities.status == context.getString(R.string.state_alive)) {
                    tvCircleFav.setBackgroundResource(R.drawable.circle)
                }

                if (characterEntities.status == context.getString(R.string.state_dead)) {
                    tvCircleFav.setBackgroundResource(R.drawable.circle_dead)
                }
                if (characterEntities.status == context.getString(R.string.state_unknown)) {
                    tvCircleFav.setBackgroundResource(R.drawable.circle_unknow)
                }
            }
        }




    }
}