package com.example.rickandmortymimo.ui.fragments.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.room.Room
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.databinding.FragmentFavoritesBinding
import com.example.rickandmortymimo.data.db.CharactersDatabase
import com.example.rickandmortymimo.data.db.entities.CharacterEntity
import com.example.rickandmortymimo.ui.activities.MainActivity
import com.example.rickandmortymimo.ui.adapters.FavoriteListAdapter



class FavoritesFragment : Fragment(R.layout.fragment_favorites) {

    private lateinit var binding: FragmentFavoritesBinding
    var adapter = FavoriteListAdapter(this)



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFavoritesBinding.inflate(layoutInflater)

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.rvCharactersfav.layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        binding.rvCharactersfav.adapter = adapter



        getCharacterFav()




    }

   fun deleteFav(characterEntity: CharacterEntity){
       val thread: Thread = object : Thread() {
           override fun run() {
               val db: CharactersDatabase = Room.databaseBuilder(
                   activity as MainActivity,
                   CharactersDatabase::class.java,
                   "characters"
               ).build()
               db.characterDao().delete(characterEntity)
           }
       }
           thread.start()
       adapter.notifyDataSetChanged()
       Toast.makeText(context, getString(R.string.favorite_delete), Toast.LENGTH_SHORT).show()
       }

    private fun getCharacterFav() {




            val thread: Thread = object : Thread() {
                override fun run() {

                    val db: CharactersDatabase = Room.databaseBuilder(
                        activity as MainActivity,
                        CharactersDatabase::class.java,
                        "characters"
                    ).build()
                    val si: List<CharacterEntity> = db.characterDao().getCharactersItems()
                    adapter.setCharacters(si)
                }
            }
            thread.start()
            adapter.notifyDataSetChanged()

        }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        getCharacterFav()

    }

    }





