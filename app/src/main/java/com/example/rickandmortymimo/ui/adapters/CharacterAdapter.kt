
package com.example.rickandmortymimo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.databinding.CustomListLayoutBinding
import com.squareup.picasso.Picasso
import com.example.rickandmortymimo.data.domain.CharacterModel
import com.example.rickandmortymimo.ui.fragments.list.CharactersListFragmentDirections
import kotlinx.android.synthetic.main.custom_list_layout.view.*

class CharacterAdapter : PagingDataAdapter<CharacterModel, CharacterAdapter.ViewHolder>(DiffUtilCallBack()) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(CustomListLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)

        holder.itemView.setOnClickListener { view ->
            val action =
                CharactersListFragmentDirections.actionNavigationCharactersToCharactersDetailsFragment(
                    getItem(position)!!
                )
            view.findNavController().navigate(action)


        }
    }



    class ViewHolder(private val binding: CustomListLayoutBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bind(characterModel: CharacterModel) {
            with(itemView) {

                Picasso.get().load(characterModel.image).into(binding.ivCharacter)

                binding.tvCharacterName.text = characterModel.name
                binding.tvState.text = characterModel.status


                if (characterModel.status == context.getString(R.string.state_alive)) {
                    tvCircle.setBackgroundResource(R.drawable.circle)
                }

                if (characterModel.status == context.getString(R.string.state_dead)) {
                    tvCircle.setBackgroundResource(R.drawable.circle_dead)
                }
                if (characterModel.status == context.getString(R.string.state_unknown)) {
                    tvCircle.setBackgroundResource(R.drawable.circle_unknow)
                }
            }
        }
    }

    class DiffUtilCallBack : DiffUtil.ItemCallback<CharacterModel>() {
        override fun areItemsTheSame(
            oldItem: CharacterModel,
            newItem: CharacterModel
        ): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(
            oldItem: CharacterModel,
            newItem: CharacterModel
        ): Boolean {
            return oldItem == newItem
        }

    }
}


