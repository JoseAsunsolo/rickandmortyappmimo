package com.example.rickandmortymimo.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.navigation.*
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.setupWithNavController
import com.example.rickandmortymimo.ui.custom.MyToolBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        getSharedPreferences()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        MyToolBar().show(this, "principal", false)

        if (savedInstanceState == null) {
            setupBottomNavigationBar()

        }
    }


    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }
    private fun getSharedPreferences(){
        val sharedPref = this.getSharedPreferences("myPrefs", Context.MODE_PRIVATE) ?: return

        when (sharedPref.getInt(getString(R.string.theme_preference), -1)) {
            1 -> {
                setTheme(R.style.ToolbarTheme_Rickandmortymimo)

            }
            0 -> {
                setTheme(R.style.Theme_Rickandmortymimo)

            }
            else -> {
                print("valor es -1")
            }
        }
    }

    private fun setupBottomNavigationBar() {
        val bottomNavigationView = btn_nav_view
        val navGraphIds = listOf(R.navigation.nav_app, R.navigation.episodes_nav, R.navigation.favorites_nav, R.navigation.images_nav)


        val controller = bottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.navHostFragment,
            intent = intent
        )


        controller.observe(this,  { navController ->

            setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, MainActivitySettings::class.java))
                true
            }
            R.id.action_info -> {
                startActivity(Intent(this, MainActivityInfo::class.java))
                true
            }else -> super.onOptionsItemSelected(item)
        }
    }

}

