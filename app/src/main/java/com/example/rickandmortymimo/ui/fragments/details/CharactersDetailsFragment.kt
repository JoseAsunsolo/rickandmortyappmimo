package com.example.rickandmortymimo.ui.fragments.details

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import android.view.View
import android.widget.Toast

import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.room.Room
import com.example.rickandmortymimo.R

import com.example.rickandmortymimo.data.db.CharactersDatabase
import com.example.rickandmortymimo.data.db.entities.CharacterEntity
import com.example.rickandmortymimo.data.domain.CharacterModel
import com.example.rickandmortymimo.ui.activities.MainActivity

import com.squareup.picasso.Picasso




import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter

import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.property.TextAlignment
import kotlinx.android.synthetic.full.fragment_details.*


class CharactersDetailsFragment : Fragment(R.layout.fragment_details) {

    private val args: CharactersDetailsFragmentArgs by navArgs()

    lateinit var db: CharactersDatabase
    private val STORAGE_CODE: Int = 100
    private val DB_NAME = "characters"
    var id: String = ""
    var name: String = " "
    var status: String = " "
    var species: String = " "
    var gender: String = " "
    var image: String = " "

    private var character: CharacterModel? = null


    companion object {
        const val REQUEST_PERMISSION = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        save()
        val character = args.characterModel

        tvStateDetails.text= character.status
        Picasso.get().load(character.image).into(ivImageCharactersDetails)
        tvNameDetails.text= character.name
        tvSpecieDetails.text = character.species
        tvGenderDetails.text = character.gender
       tvLocationDetails.text = character.locationCharacter.name





        if (character.status == getString(R.string.state_alive)) {
            tvCircleDetails.setBackgroundResource(R.drawable.circle)
        }

        if (character.status == getString(R.string.state_dead)) {
            tvCircleDetails.setBackgroundResource(R.drawable.circle_dead)
        }
        if (character.status == getString(R.string.state_unknown)) {
            tvCircleDetails.setBackgroundResource(R.drawable.circle_unknow)
        }

        requireView().findViewById<View>(R.id.btnPDF).setOnClickListener {
            if (checkSelfPermission(this.requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION)
            } else {
                savePdf(character)
            }
        }

    }

    @Suppress("DEPRECATION")
    private fun savePdf(character: CharacterModel) {

        val mFileName = character.name //SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis())
        val mFilePath = Environment.getExternalStorageDirectory().toString() + "/" + mFileName +".pdf"
        val pdfDocument = PdfDocument(PdfWriter(mFilePath))
        pdfDocument.defaultPageSize = PageSize.A6

        try {

            val mDoc = Document(pdfDocument)

            val name = "NAME: " + character.name
            val gender = "GENDER: " + character.gender
            val status = "STATUS: " + character.status
            val location = "LAST KNOWN LOCATION: " + character.locationCharacter.name
            val species = "SPECIES: " + character.species

            mDoc.add(Paragraph(character.name).setFontSize(20.0f).setTextAlignment(TextAlignment.CENTER).setBold().setMarginBottom(40.0f))
            mDoc.add(Paragraph(name))
            mDoc.add(Paragraph(gender))
            mDoc.add(Paragraph(status))
            mDoc.add(Paragraph(species))
           mDoc.add(Paragraph(location))

            mDoc.close()
            
            Toast.makeText(context, "$mFileName.pdf\nsaved to\n$mFilePath", Toast.LENGTH_SHORT).show()
        }
        catch (e: Exception){
            //if anything goes wrong causing exception, get and show exception message
            Toast.makeText(context,"Error: " + e.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            REQUEST_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted, call savePdf() method
                    Toast.makeText(context, "PERMISSION GRANTED, DOWNLOADING PDF", Toast.LENGTH_SHORT).show()
                    this.character?.let { savePdf(it) }
                }
                else{
                    //permission from popup was denied, show error message
                    Toast.makeText(context , "Permission denied...!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun save() {
        requireView().findViewById<View>(R.id.btnSave)
            .setOnClickListener {

                val thread: Thread = object : Thread() {
                    override fun run() {
                        db = Room.databaseBuilder(
                            activity as MainActivity,
                            CharactersDatabase::class.java,
                            DB_NAME
                        ).build()
                        Looper.prepare()

                        val characterItem = CharacterEntity(id.toInt(), name, status, species, gender, image)
                        val character = args.characterModel
                        characterItem.id = character.id
                        characterItem.name = character.name
                        characterItem.status = character.status
                        characterItem.species = character.species
                        characterItem.image = character.image
                        db.characterDao().insert(characterItem)



                    }

                }

                Toast.makeText(activity, getString(R.string.character_favorite_save), Toast.LENGTH_SHORT).show()
                thread.start()
            }
    }

}


