package com.example.rickandmortymimo.ui.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.ui.custom.MyToolBar
import kotlinx.android.synthetic.main.fragment__custom_info.*

class MainActivityInfo : AppCompatActivity() {

    private val URLGITLAB = "https://gitlab.com/JoseAsunsolo/rickandmortyappmimo"
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        MyToolBar().show(this, getString(R.string.label_settings), true)
        btnGitHub.setOnClickListener{
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            intent.data = Uri.parse(URLGITLAB)
            ContextCompat.startActivity(this, intent, Bundle())

        }
    }
}