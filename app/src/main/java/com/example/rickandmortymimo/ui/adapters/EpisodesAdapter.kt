package com.example.rickandmortymimo.ui.adapters

import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmortymimo.R
import com.example.rickandmortymimo.databinding.ItemsListEpisodesBinding
import com.example.rickandmortymimo.data.domain.Episode
import com.example.rickandmortymimo.ui.fragments.list.EpisodesListFragmentDirections

import kotlinx.android.synthetic.main.items_list_episodes.view.*

@Suppress("DEPRECATION")
class EpisodesAdapter : PagingDataAdapter<Episode, EpisodesAdapter.ViewHolder>(DiffUtilCallBack()){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemsListEpisodesBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)

        holder.itemView.setOnClickListener { view->
            val action = EpisodesListFragmentDirections.actionNavigationEpisodesToEpisodesDetailFragment(getItem(position)!!)
            view.findNavController().navigate(action)
        }
    }


    class ViewHolder(binding: ItemsListEpisodesBinding) : RecyclerView.ViewHolder(binding.root){


        fun bind(episode: Episode) {
            with(itemView) {

                episodeName.text = episode.name
                episodeNumber.text = episode.episode_number
                imgSeason.setColorFilter(resources.getColor(R.color.light_green), PorterDuff.Mode.SRC_ATOP)
                if (episode.episode_number.contains(context.getString(R.string.season_1))){
                    imgSeason.setImageResource(R.drawable.number1)

                }
                if (episode.episode_number.contains(context.getString(R.string.season_2))){
                    imgSeason.setImageResource(R.drawable.number2)
                }
                if (episode.episode_number.contains(context.getString(R.string.season_3))){
                    imgSeason.setImageResource(R.drawable.number3)
                }
                if (episode.episode_number.contains(context.getString(R.string.season_4))){
                    imgSeason.setImageResource(R.drawable.number4)
                }

            }
        }
    }

    class DiffUtilCallBack : DiffUtil.ItemCallback<Episode>() {
        override fun areItemsTheSame(
            oldItem: Episode,
            newItem: Episode
        ): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(
            oldItem: Episode,
            newItem: Episode
        ): Boolean {
            return oldItem == newItem
        }

    }

}