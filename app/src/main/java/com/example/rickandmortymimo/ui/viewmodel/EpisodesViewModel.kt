package com.example.rickandmortymimo.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.rickandmortymimo.data.domain.Episode
import com.example.rickandmortymimo.data.network.repository.EpisodeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class EpisodesViewModel @Inject constructor(private val repository: EpisodeRepository): ViewModel() {

    fun getListData() : Flow<PagingData<Episode>> {
        return repository.getEpisode().cachedIn(viewModelScope)
    }







}