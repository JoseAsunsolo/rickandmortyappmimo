package com.example.rickandmortymimo.ui.custom

import androidx.appcompat.app.AppCompatActivity
import com.example.rickandmortymimo.R

class MyToolBar {
    fun show(activities : AppCompatActivity, title: String, upButton: Boolean){
        activities.setSupportActionBar(activities.findViewById(R.id.toolbar))
        activities.supportActionBar?.title = title
        activities.supportActionBar?.setDisplayHomeAsUpEnabled(upButton)

    }

}