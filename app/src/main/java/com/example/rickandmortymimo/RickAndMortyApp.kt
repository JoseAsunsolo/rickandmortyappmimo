package com.example.rickandmortymimo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RickyAndMortyApp : Application()