package com.example.rickandmortymimo.data.db

import androidx.room.*
import com.example.rickandmortymimo.data.db.entities.CharacterEntity



@Dao
interface CharacterDao {
    @Query("SELECT * FROM CharacterEntity")
    fun getCharactersItems(): List<CharacterEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(characterEntity: CharacterEntity)

    @Delete
    fun delete(characterEntity: CharacterEntity)
}
