package com.example.rickandmortymimo.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rickandmortymimo.data.db.entities.CharacterEntity



@Database(entities = [CharacterEntity::class], version = 1, exportSchema = false)
abstract class CharactersDatabase : RoomDatabase() {
    abstract fun characterDao(): CharacterDao


    }