package com.example.rickandmortymimo.data.network.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.rickandmortymimo.data.domain.CharacterModel
import com.example.rickandmortymimo.data.network.ApiService
import com.example.rickandmortymimo.ui.adapters.CharacterPagingSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class CharacterRepository @Inject constructor(private val apiService: ApiService){
    fun getCharacter() : Flow<PagingData<CharacterModel>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 35),
            pagingSourceFactory = {
                CharacterPagingSource(apiService)
            }
        ).flow
    }
}