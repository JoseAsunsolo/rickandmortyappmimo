package com.example.rickandmortymimo.data.domain

data class Info (
    val count: Long,
    val pages: Long,
    val next: String?,
    val prev: String?
)