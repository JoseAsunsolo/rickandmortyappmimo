package com.example.rickandmortymimo.data.domain



data class CharacterResponse(
    val info: Info,
    val results: List<CharacterModel>
)