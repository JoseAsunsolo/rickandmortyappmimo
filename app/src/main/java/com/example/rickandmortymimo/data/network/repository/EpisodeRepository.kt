package com.example.rickandmortymimo.data.network.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.rickandmortymimo.data.domain.Episode
import com.example.rickandmortymimo.data.network.ApiService
import com.example.rickandmortymimo.ui.adapters.EpisodesPagingSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class EpisodeRepository  @Inject constructor(private val apiService: ApiService){
    fun getEpisode() : Flow<PagingData<Episode>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 4),
            pagingSourceFactory = {
                EpisodesPagingSource(apiService)
            }
        ).flow
    }
}