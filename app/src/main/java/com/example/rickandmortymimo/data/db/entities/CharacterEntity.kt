package com.example.rickandmortymimo.data.db.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class CharacterEntity(
    @PrimaryKey var id: Int? = null,
    var name: String = "",
    var status: String = "",
    var species: String = "",
    var gender: String = "",
    var image: String = "",
) : Parcelable {}