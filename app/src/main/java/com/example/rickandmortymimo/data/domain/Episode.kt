package com.example.rickandmortymimo.data.domain

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Episode(
    val id: Int,
    val name: String,
    val air_date: String,
    @SerializedName("episode") val episode_number: String
) : Parcelable