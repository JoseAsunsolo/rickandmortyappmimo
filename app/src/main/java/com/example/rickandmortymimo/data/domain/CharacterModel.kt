package com.example.rickandmortymimo.data.domain

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
data class CharacterModel(
    @PrimaryKey var id: Int,
    var name: String,
    var status: String,
    @SerializedName("location") val locationCharacter: LocationCharacter,
    var species: String,
    val gender: String,
    var image: String
    ) : Parcelable



