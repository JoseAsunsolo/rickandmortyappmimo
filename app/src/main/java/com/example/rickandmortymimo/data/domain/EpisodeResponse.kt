package com.example.rickandmortymimo.data.domain



data class EpisodeResponse(
    val info: Info,
    val results: List<Episode>
)