package com.example.rickandmortymimo.data.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class LocationCharacter (
    val name : String) : Parcelable
